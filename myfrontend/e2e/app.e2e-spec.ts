import { MyfrontendPage } from './app.po';

describe('myfrontend App', () => {
  let page: MyfrontendPage;

  beforeEach(() => {
    page = new MyfrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
