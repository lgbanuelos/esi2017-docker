import { Component } from "@angular/core";
import { Http } from "@angular/http";

@Component({
    selector: 'query',
    templateUrl: './plant-catalog-query.component.html'
})
export class PlantCatalogQueryComponent {
    name: string;
    startDate: Date;
    endDate: Date;
    plants: any[];

    constructor(private http: Http){}
    query() {
        this.http.get( 'http://localhost:8080/api/inventory/plants')
            .subscribe( resp => this.plants = resp.json());
    }
}