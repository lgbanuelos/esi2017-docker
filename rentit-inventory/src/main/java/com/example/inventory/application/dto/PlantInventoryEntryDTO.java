package com.example.inventory.application.dto;


import lombok.Data;

import java.math.BigDecimal;

@Data
public class PlantInventoryEntryDTO {
    Long _id;
    String name;
    String description;
    BigDecimal price;
}
