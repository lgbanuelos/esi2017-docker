package com.example.inventory.application.service;

import com.example.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.inventory.domain.PlantInventoryEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InventoryService {
    @Autowired
    PlantInventoryEntryRepository repository;
    @Autowired
    PlantInventoryEntryAssembler assembler;

    public List<PlantInventoryEntryDTO> findAll() {
        return assembler.toResources(repository.findAll());
    }
}
