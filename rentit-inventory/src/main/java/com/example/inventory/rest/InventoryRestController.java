package com.example.inventory.rest;

import com.example.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.inventory.application.service.InventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class InventoryRestController {
    @Autowired
    InventoryService service;

    @GetMapping("/api/inventory/plants")
    public List<PlantInventoryEntryDTO> findAll() {
        return service.findAll();
    }

}
